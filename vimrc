
set number	
set linebreak	
set showbreak=+++	
set textwidth=100	
set showmatch	
 
set hlsearch	
set smartcase	
set ignorecase	
set incsearch	
 
set autoindent	
set cindent	
set shiftwidth=4	
set smartindent	
set smarttab	
set softtabstop=0	
 

set ruler	
set showtabline=2	
set cmdheight=2	
 
set autowriteall	
set backupdir=~/.vim/backups	
 
set undolevels=1000	
set backspace=indent,eol,start	
syntax on
