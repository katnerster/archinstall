#!/bin/bash

MOUNTPOINT="/mnt/klop"
GRUBTHEME="grubtheme.7z"
SLIMTHEME="slimtheme.7z"
HOST_NAME="x230"
ROOT_PASSWD="kernel"
USER="michal"
USER_PASSWD="kernel"
LOCALE="pl_PL.UTF-8"

arch_chroot() {
    arch-chroot $MOUNTPOINT /bin/bash -x -c "${1}"
}  
check_for_error() {

 if [[ $? -eq 1 ]] && [[ $(cat /tmp/.errlog | grep -i "error") != "" ]]; then
    echo "" > /tmp/.errlog
    echo "FATAL"
    exit
 fi
   
}

pacman-key --init 2>/tmp/.errlog
pacman-key --populate archlinux 2>/tmp/.errlog
pacman-key --refresh-keys 2>/tmp/.errlog

echo -e "Server = http://ftp.u-tx.net/pub/Linux/dist/archlinux/\$repo/os/\$arch\n$(cat /etc/pacman.d/mirrorlist)" > /etc/pacman.d/mirrorlist
echo -e "Server = http://ftp.icm.edu.pl/pub/Linux/dist/archlinux/\$repo/os/\$arch\n$(cat /etc/pacman.d/mirrorlist)" > /etc/pacman.d/mirrorlist

clear
echo "Installing base and base-devel"
pacstrap ${MOUNTPOINT} base base-devel btrfs-progs ntp sudo  2>/tmp/.errlog

echo -e "Server = http://ftp.u-tx.net/pub/Linux/dist/archlinux/\$repo/os/\$arch\n$(cat /etc/pacman.d/mirrorlist)" > ${MOUNTPOINT}/etc/pacman.d/mirrorlist
echo -e "Server = http://ftp.icm.edu.pl/pub/Linux/dist/archlinux/\$repo/os/\$arch\n$(cat ${MOUNTPOINT}/etc/pacman.d/mirrorlist)" > ${MOUNTPOINT}/etc/pacman.d/mirrorlist

sed -i 's/.pkg.tar.xz/pkg.tar/g' ${MOUNTPOINT}/etc/makepkg.conf

echo "Installing GRUB2"
pacstrap ${MOUNTPOINT} grub os-prober 2>/tmp/.errlog
#grub-install /dev/sda --root=/mnt
#http://vinceliuice.deviantart.com/art/Grub-themes-vimix-0-1-532580485
pacman --noconfirm -S p7zip

mkdir ${MOUNTPOINT}/boot/grub/theme/ 2>/tmp/.errlog
7z e ${GRUBTHEME} -o${MOUNTPOINT}/boot/grub/theme/ 2>/tmp/.errlog
sed -i 's/#GRUB_THEME="\/path\/to\/gfxtheme"/GRUB_THEME="\/boot\/grub\/theme\/theme.txt"/' ${MOUNTPOINT}/etc/default/grub 2>/tmp/.errlog
arch_chroot "grub-mkconfig -o /boot/grub/grub.cfg" 2>/tmp/.errlog
#grub-install /dev/sda --root=/mnt

echo "Installing yaourt"
echo "[archlinuxfr]" >> ${MOUNTPOINT}/etc/pacman.conf
echo "SigLevel = Never" >> ${MOUNTPOINT}/etc/pacman.conf
echo "Server = http://repo.archlinux.fr/\$arch" >> ${MOUNTPOINT}/etc/pacman.conf
arch_chroot "pacman --noconfirm -Sy yaourt"

 sed -i 's/#NOCONFIRM=0/NOCONFIRM=1/g' ${MOUNTPOINT}/etc/yaourtrc 

#Język
echo "LANG=\"${LOCALE}\"" > ${MOUNTPOINT}/etc/locale.conf
sed -i "s/#${LOCALE}/${LOCALE}/" ${MOUNTPOINT}/etc/locale.gen 
arch_chroot "locale-gen" >/dev/null  2>/tmp/.errlog

#Czas
arch_chroot "ln -s /usr/share/zoneinfo/Europe/Warsaw /etc/localtime" 2>/tmp/.errlog
arch_chroot "hwclock --systohc --utc"  2>/tmp/.errlog

#fstab
genfstab -U -p ${MOUNTPOINT} >> ${MOUNTPOINT}/etc/fstab 2>/tmp/.errlog

#hostname
echo "$HOST_NAME" > ${MOUNTPOINT}/etc/hostname 2>/tmp/.errlog
sed -i "/127.0.0.1/s/$/ ${HOST_NAME}/" ${MOUNTPOINT}/etc/hosts
sed -i "/::1/s/$/ ${HOST_NAME}/" ${MOUNTPOINT}/etc/hosts

#hasło roota
echo -e "${ROOT_PASSWD}\n${ROOT_PASSWD}" > /tmp/.passwd
arch_chroot "passwd root" < /tmp/.passwd >/dev/null 2>/tmp/.errlog
rm /tmp/.passwd

#nowy użytkonik
arch_chroot "useradd ${USER} -m -g users -G wheel,storage,power,network,video,audio,lp -s /bin/bash" 2>/tmp/.errlog
echo -e "${USER_PASSWD}\n${USER_PASSWD}" > /tmp/.passwd
arch_chroot "passwd ${USER}" < /tmp/.passwd >/dev/null 2>/tmp/.errlog
rm /tmp/.passwd
arch_chroot "cp /etc/skel/.bashrc /home/${USER}"
arch_chroot "chown -R ${USER}:users /home/${USER}"
sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^#//' ${MOUNTPOINT}/etc/sudoers

#wifi
pacstrap ${MOUNTPOINT} iw wireless_tools wpa_actiond wpa_supplicant dialog 

#xorg
pacstrap ${MOUNTPOINT} alsa-utils xorg-server xorg-server-utils xorg-xinit xf86-input-synaptics xf86-input-keyboard xf86-input-mouse 2>/tmp/.errlog
pacstrap ${MOUNTPOINT} xf86-video-intel libva-intel-driver intel-ucode 2>/tmp/.errlog
sed -i 's/MODULES=""/MODULES="i915"/' ${MOUNTPOINT}/etc/mkinitcpio.conf

#awesomewm
pacstrap ${MOUNTPOINT} git
pacstrap ${MOUNTPOINT} awesome vicious polkit-gnome dmenu 
pacstrap ${MOUNTPOINT} xterm gksu gnome-keyring polkit xdg-user-dirs xdg-utils gamin gvfs gvfs-afc gvfs-smb ttf-dejavu gnome-icon-theme python2-xdg bash-completion ntfs-3g ttf-font-icons
arch_chroot "sudo -u ${USER} git clone https://katnerster@bitbucket.org/katnerster/awesome-wm-config.git /home/${USER}/.config/awesome"
arch_chroot "cd /home/${USER}/.config/awesome; sudo -u ${USER} bash -x setup.sh"
arch_chroot "sudo -u ${USER} git clone https://github.com/copycat-killer/lain.git /home/${USER}/.config/awesome/lain"
arch_chroot "sudo -u ${USER} mkdir ~/.config/awesome/lain"
arch_chroot "cd /home/${USER}/.config/awesome/lain; git reset --hard 69ddbc74a59ead7b3c140d42d94cb39d574284fa"
cp icons.ttf ${MOUNTPOINT}/usr/share/fonts/TTF



#slim
pacstrap ${MOUNTPOINT} slim 2>/tmp/.errlog
arch_chroot "systemctl enable slim.service" >/dev/null 2>>/tmp/.errlog
DM="SLiM"
# Amend the xinitrc file accordingly for all user accounts
user_list=$(ls ${MOUNTPOINT}/home/ | sed "s/lost+found//")
for i in ${user_list[@]}; do
  if [[ -n ${MOUNTPOINT}/home/$i/.xinitrc ]]; then
    cp -f ${MOUNTPOINT}/etc/skel/.xinitrc ${MOUNTPOINT}/home/$i
    arch_chroot "chown -R ${i}:users /home/${i}"
  fi
  echo 'exec $1' >> ${MOUNTPOINT}/home/$i/.xinitrc
done
7z e ${SLIMTHEME} -o${MOUNTPOINT}/usr/share/slim/themes/
mv ${MOUNTPOINT}/usr/share/slim/themes/*.{png,theme} ${MOUNTPOINT}/usr/share/slim/themes/Bamboo_Stones
sed -i 's/current_theme       default/current_theme Bamboo_Stones/' ${MOUNTPOINT}/etc/slim.conf
sed -i 's/session_font.*/session_font		LMMonoCaps10:size=0/g' ${MOUNTPOINT}/usr/share/slim/themes/Bamboo_Stones/slim.theme



#networkmanager
pacstrap ${MOUNTPOINT} networkmanager network-manager-applet rp-pppoe 2>/tmp/.errlog
arch_chroot "systemctl enable NetworkManager.service && systemctl enable NetworkManager-dispatcher.service" 2>>/tmp/.errlog

#zsh
pacstrap ${MOUNTPOINT} zsh
cp zshrc ${MOUNTPOINT}/home/${USER}/.zshrc
arch_chroot "sudo -u ${USER} chsh /usr/bin/zsh"
cp xresources ${MOUNTPOINT}/home/${USER}/.Xresources
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-hack"

#chrome
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S google-chrome ttf-liberation"

#deadbeef
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S deadbeef deadbeef-plugin-fb"

#vim
pacstrap ${MOUNTPOINT} vim
cp vimrc ${MOUNTPOINT}/home/${USER}/.vimrc
arch_chroot "sudo -u ${USER} mkdir -p ~/.vim/backups"

#mplayer
pacstrap ${MOUNTPOINT} mplayer
mkdir ${MOUNTPOINT}/home/${USER}/.mplayer/
echo -e "subcp=cp1250\nsoftvol=yes\nsoftvol-max=400" > ${MOUNTPOINT}/home/${USER}/.mplayer/config

pacstrap ${MOUNTPOINT} feh ranger file-roller libreoffice-fresh libreoffice-fresh-pl xclip acpi acpid filelight
pacstrap ${MOUNTPOINT} termite jdk8-openjdk jre8-openjdk jre8-openjdk-headless terminus-font libnotify

arch_chroot "chown -R ${USER}:users /home/${USER}"

cp xorg/* ${MOUNTPOINT}/etc/X11/xorg.conf.d
cp -r /etc/NetworkManager/system-connections ${MOUNTPOINT}/etc/NetworkManager/

#udiskie
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S acpi"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S acpid"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S adobe-source-code-pro-fonts"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S adwaita-icon-theme"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S apulse"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S artwiz-fonts"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S aspell"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S aspell-pl"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S at"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S audacity"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S bash-completion"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S bashmount"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S bc"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S bdf-unifont"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S bluez"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S bluez-libs"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S bluez-utils"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S boost"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S boost-libs"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S cairo-infinality-ultimate"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S cifs-utils"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S cpupower"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S cups"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S cups-filters"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S cups-pdf"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S cvs"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S deluge"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S dhclient"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S dkms"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S feh"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ffmpeg"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ffmpeg-compat"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ffmpegthumbnailer"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S file-roller"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S filelight"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S filezilla"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S firefox"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S flashplugin"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S fontconfig-infinality-ultimate"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S freetype2-infinality-ultimate"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S fuse"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S fuse-exfat"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gdb"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S git"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gitg"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gparted"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gst-plugins-bad"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gst-plugins-base"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gst-plugins-base-libs"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gst-plugins-good"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gst-plugins-ugly"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-bad"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-bad-plugins"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-base"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-base-plugins"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-ffmpeg"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-good"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-good-plugins"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-ugly"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gstreamer0.10-ugly-plugins"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gtk-engine-murrine"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gtk-engines"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gtk-theme-boje"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gtk-update-icon-cache"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gtk2"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gtk3"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S gvim"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S hplip"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S hspell"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S htop"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S httpie"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S hunspell"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S hunspell-pl"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S hyphen"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S imagemagick"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ipython"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ipython2"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S leafpad"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lib32-cairo-infinality-ultimate"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lib32-fontconfig-infinality-ultimate"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lib32-freetype2-infinality-ultimate"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S libreoffice-fresh"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S libreoffice-fresh-pl"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lm_sensors"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lsof"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lua"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lua-lgi"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lua-lpeg"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S lxappearance"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S modem-manager-gui"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S modemmanager"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S muparser"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S mupdf"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S napi-bash"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S network-manager-applet"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S networkmanager"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S networkmanager-dispatcher-ntpd"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S nm-connection-editor"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ntfs-3g"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ntp"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S numix-themes"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S oblogout"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ocl-icd"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S os-prober"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S otf-hack"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S otf-powerline-symbols-git"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S otf-source-code-pro-powerline-git"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S oxygen-icons"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S p4v"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S p7zip"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S pcmanfm"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S phonon-qt4"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S phonon-qt4-gstreamer"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S phonon-qt5"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S phonon-qt5-gstreamer"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S pkgstats"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S powertop"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S python"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S python-pip"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S python-setuptools"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S python2"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S python2-pip"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S qt4"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ranger"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S rsync"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ruby"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S samba"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S screen"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S scrot"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S shared-mime-info"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S simple-scan"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S skype"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S smbclient"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S sshfs"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S strace"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S subversion"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S teamviewer"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S terminus-font"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S tk"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S tlp"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S tmux"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S tree"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-bitstream-vera"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-dejavu"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-font-icons"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-funfonts"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-liberation"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-monofur"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-ms-fonts"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-ubuntu-font-family"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S ttf-vista-fonts"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S udiskie"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S udisks2"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S unrar"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S unzip"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S v4l-utils"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S vertex-themes"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S vim-runtime"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S vlc"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S vnstat"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S volnoti"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S vte"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S vte-common"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S vte3-ng"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S w3m"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S wget"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S wmname"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S wxgtk"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S wxgtk2.8"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S x265"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-bdftopcf"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-font-util"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-font-utils"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-fonts-alias"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-fonts-encodings"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-fonts-misc"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-iceauth"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-luit"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-mkfontdir"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-mkfontscale"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-server"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-server-common"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-server-utils"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-server-xephyr"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-server-xwayland"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-sessreg"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-setxkbmap"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xauth"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xbacklight"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xcmsdb"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xev"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xgamma"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xhost"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xinit"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xinput"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xkbcomp"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xkill"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xmessage"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xmodmap"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xprop"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xrandr"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xrdb"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xrefresh"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xset"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xsetroot"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xorg-xwininfo"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xterm"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S xvidcore"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S yelp-xsl"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S youtube-dl"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S zathura-git"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S zathura-pdf-mupdf-git"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S zip"
arch_chroot "sudo -u ${USER} yaourt --noconfirm -S zuki-themes-git"

arch_chroot "sudo -u ${USER} ranger --copy-config=all"
echo "map D delete" >> ${MOUNTPOINT}/home/${USER}/.config/ranger/rc.conf
arch_chroot 'sudo -u ${USER} sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"'

cat zshrc >> ${MOUNTPOINT}/home/${USER}/.zshrc
