ZSH_THEME="robbyrussell"
ENABLE_CORRECTION="true"
plugins=(git)
MESA_GLSL_VERSION_OVERRIDE=130
export QT_MESSAGE_PATTERN="[%{type}] %{appname} (%{file}:%{line}) - %{message}"
alias make='make -j5'

function ranger-cd {
    tempfile='/tmp/chosendir'
    /usr/bin/ranger --choosedir="$tempfile" "${@:-$(pwd)}"
    test -f "$tempfile" &&
    if [ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]; then
        cd -- "$(cat "$tempfile")"
    fi
    rm -f -- "$tempfile"
}

ttyctl -f  #Naprawa ^M
foreground-vi() {
  fg %vim
}
zle -N foreground-vi
bindkey '^Z' foreground-vi
export EDITOR=/bin/vim

ranger_cd() { BUFFER="ranger-cd"; zle accept-line; } 
zle -N ranger_cd
bindkey '^N' ranger_cd 

export TIME_STYLE=long-iso

